import sys
import struct
import nuke, os, errno

def changeMovColorAtoms(primaries, tranfer, c_matrix):
    fileName = nuke.selectedNode().knob('file').getValue()
    print(fileName)
    with open(fileName, "rb+") as input_file:
        fileData = input_file.read()
        try:
            print("Changing colr atoms")
            colr_atom_index = fileData.index(b'colrnclc')
            print(colr_atom_index)

            # Seek to folor atom offset and read current values
            offset = hex(colr_atom_index)
            input_file.seek(colr_atom_index + 8, 0)
            color_primaries = struct.unpack('>h', input_file.read(2))[0]
            transfer_function = struct.unpack('>h', input_file.read(2))[0]
            color_matrix = struct.unpack('>h', input_file.read(2))[0]
            print("Color primaries: " + str(color_primaries))
            print("Transfer function: " + str(transfer_function))
            print("Color matrix: " + str(color_matrix))
    
            # Seek back to colr atom start and write new values
            input_file.seek(colr_atom_index + 8, 0)
            input_file.write(struct.pack('>h', primaries))
            input_file.write(struct.pack('>h', tranfer))
            input_file.write(struct.pack('>h', c_matrix))
    
            # Seek to start of colr atom again and check applied values
            input_file.seek(colr_atom_index + 8, 0)
            color_primaries = struct.unpack('>h', input_file.read(2))[0]
            transfer_function = struct.unpack('>h', input_file.read(2))[0]
            color_matrix = struct.unpack('>h', input_file.read(2))[0]
            print("New Color primaries: " + str(color_primaries))
            print("New Transfer function: " + str(transfer_function))
            print("New Color matrix: " + str(color_matrix))
    
        except ValueError:
            print('Invalid file')

# Change the numbers here for different colr atom parameters
changeMovColorAtoms(1, 1, 1)